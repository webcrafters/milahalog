var batch_no = "", no_of_inv = "", batch_amt = "", customer_id = "", customer_name = "", received_by = "", comments = "", userid = "";
var user_name="";
var scanned_id;
$(function () {
    $(document).on('keyup', '.batchno', function () {
        if ($(".batchno").val() != "") {
            $(".event_scan").prop('disabled', 'true');
        } else {
            $(".event_scan").removeAttr('disabled');
        }
    });

    $(document).on('keydown', '.user', function (event) {

        if (event.keyCode == 13) {
            $(".btnLogin").trigger("click");
        }
    });

    $(document).on('keydown', '.pass', function (event) {

        if (event.keyCode == 13) {
            $(".btnLogin").trigger("click");
        }
    });
    
    $(document).on('keydown', '.batchno', function (event) {

        if (event.keyCode == 13) {
            $(".eventView").trigger("click");
        }
    });
    
    $(document).on('keydown', '.received_by', function (event) {

        if (event.keyCode == 13) {
            $(".confirm_event").trigger("click");
        }
    });
    
    $(document).on('keydown', '.comments', function (event) {

        if (event.keyCode == 13) {
            $(".confirm_event").trigger("click");
        }
    });

    $(".event_scan").click(function () {
//    alert("heh");
        cloudSky.zBar.scan({}, onEventSuccess, onEventFailure);
    });

    function onEventSuccess(text) {      
        //scanned_id = text;
        $(".batchno").val(text);
    } 
    function onEventFailure(reason) {
        alert("QR Code could not be scanned. Please try again");
    }
});

var currentTab = 0;

function changePage(left) {
//                            alert("Hello");
    var page = "";
    switch (currentTab) {
        case 0:
            page = "#loginpage";
            break;
        case 1:
            page = "#resetpage";
            break;
        case 2:
            page = "#invoice_updates";
            break;

        default:
            page = "#loginpage";
    }
    $.mobile.changePage(page, {
        transition: 'slide',
        reverse: left
    });
}
$

//var ENDPOINTURL = "http://182.72.197.202:8080/idl/";
var ENDPOINTURL = "https://aznvoccweb1.qatarnav.com/qnless/wss/";
$(function () {

    //2-1

    $(".btnLogin").click(function () {
        $(".pbar").show();
        userid = $(".user").val();
        var password = $(".pass").val();
        if (userid == "" || password == "")
        {
            $(".pbar").hide();
            alert("Pls fill in all the fields");
        } else
        {
            var data = {
                'username': userid,
                'password': password
            };
            $.ajax({
                type: "POST",
                url: ENDPOINTURL + "checklogin.php",
                data: data,
                dataType: "json",
                success: function (successData, status, xhr) {

                    console.log(successData);
                    if (successData.auth == 0)
                    {
                        $(".btnLogin").hide();
                        $(".pbar").show();
                        setTimeout(function () {
                            $(".pbar").hide();
                            $(".btnLogin").show();
                        }, 1000);
                        alert(successData.error_msg);
                    }
                    else
                    {
                        user_name = successData.username;
                       
                   
                        $(".pbar").hide();
                        $(".btnLogin").hide();
                        userid = successData.userid;
                        $("#loginpage").hide();
                        setTimeout(function () {
                            $("#resetpage").show();
                        }, 1000);
                        currentTab = 1;
                        changePage(false);
                    }
                    $(".user").val("");
                    $(".pass").val("");
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

//    $(".clear_actualevents").click(function () {
//        $(".received_by").val("");
//        $(".comments").val("");
//    });

    //2-2
    $(".eventView").click(function () {
        scanned_id = $(".batchno").val();
        $(".pbar").show();
        var data = {
          'userid': userid,  
          'batch_no': scanned_id
        };
        $.ajax({
            type: "POST",
            url: ENDPOINTURL + "get_invoice_dtls.php",
            data: data,
            dataType: "json",
            success: function (successData, status, xhr) { 
                console.log(successData);
                if (successData.error_msg != "")
                {
                    $(".pbar").hide();
                    alert(successData.error_msg);
                } else
                {
                    $(".pbar").hide();
                    //clear values
                    $(".batchnumber").val("");
                    //$(".batchdate").val("");
                    //$(".customer_id").val("");
                    $(".customer_n").val("");
                    $(".noofinvoices").val("");
                    $(".totamt").val("");
                    $(".received_by").val("");
                    $(".comments").val("");
                    
                    //assign values
                    batch_no = successData.batch_no;
                    no_of_inv = successData.no_of_invoices;
                    batch_amt = successData.total_Amt
                    customer_id = successData.customer_id
                    customer_name = successData.customer_name
                    
                    $(".batchnumber").val(successData.batch_no);
                    //$(".batchdate").val("");
                    //$(".customer_id").val(successData.customer_id);
                    $(".customer_n").val(successData.customer_name);
                    $(".noofinvoices").val(successData.no_of_invoices);
                    $(".totamt").val(successData.total_Amt);
                    $(".received_by").val(successData.received_by);
                    $(".comments").val(successData.comments);
                    
                    $("#resetpage").hide();
                    $(".batchno").val("");
                    setTimeout(function () {
                    $("#invoice_updates").show();
                    }, 1000);
                    currentTab = 2;
                    changePage(false);   
                }
                },
            error: function (error) {
                console.log(error);
            }
        });
    });

    //2-3
    $(".confirm_event").click(function () {
        $(".event_scan").removeAttr('disabled');
        var rcv = $(".received_by").val();
        var cmts = $(".comments").val();
        $(".pbar").show();
        if ( rcv== "")
        {
            $(".pbar").hide();
            alert("Pls fill in all the fields");
        }
        else
        {
            
            comments = $(".comments").val();
            received_by = $(".received_by").val();
            var data = {
                'userid': userid,
                'batch_no': batch_no,
//                'no_of_invoices': no_of_inv,
//                'total_Amt': batch_amt,
//                'customer_id': customer_id,
//                'customer_name': customer_name,
                'comments': comments,
                'received_by': received_by
            };
            $.ajax({
                type: "POST",
                url: ENDPOINTURL + "update_invoice_dtls.php",
                data: data,
                dataType: "json",
                success: function (successData, status, xhr) {

                    console.log(successData);
                    if (successData.error_msg != "")
                    {
                        $(".pbar").show();
                        setTimeout(function () {
                            $(".pbar").hide();
                        }, 1000);
                        alert(successData.error_msg);
                    } else
                    {
                        $(".pbar").hide();
                        alert("Details has been updated");
                        $("#invoice_updates").hide();
                        $(".event_scan").removeAttr('disabled');
                        currentTab = 1;
                        changePage(true);
                        $("#resetpage").show();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        ;
    });

    $(".clear_actualevents").click(function () {
        $("#invoice_updates").hide();
        currentTab = 1;
        changePage(true);
        $("#resetpage").show();
        $(".event_scan").removeAttr('disabled');
    });

       $(".logoutb").click(function ()
       {
           alert("logged out");
           userid="";
           if(currentTab==1)
           {
               $("#resetpage").hide();
               currentTab=0;
               changePage(true);
                $("#loginpage").show();
                $(".btnLogin").show();
           }
           if(currentTab==2)
           {
               $("#invoice_updates").hide();   
               currentTab=0;
               changePage(true);
               $("#loginpage").show();
               $(".btnLogin").show();
           }
       });
});